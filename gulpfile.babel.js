import gulp from 'gulp';
import del from 'del';
import webpack from 'webpack';
import webpackStream from 'webpack-stream';
import named from 'vinyl-named';
import hb from 'gulp-hb';
import plumber from 'gulp-plumber';
import rename from 'gulp-rename';
import beautify from 'gulp-beautify';
import gulpIf from 'gulp-if';
import filter from 'gulp-filter';
import svgMin from 'gulp-svgmin';
import svgStore from 'gulp-svgstore';
import svgSprite from 'gulp-svg-sprite';
import tap from 'gulp-tap';
import merge from 'merge-stream';
import inject from 'gulp-inject';
import browserSync from 'browser-sync';
import sort from 'gulp-sort';
import {argv} from 'yargs';

import fs from 'fs';
import path from 'path';

import config from './config';

const isProd = !!argv.production;
const isDev = !isProd;
const inputDir = config.inputDir;
const outputDir = config.outputDir;

const clean = () => del([config.outputDir]);

const buildWebpack = () => {
  return gulp.src([`./${inputDir}/js/main.js`, `./${inputDir}/styles/main.scss`])
    .pipe(plumber())
    .pipe(named())
    .pipe(webpackStream(require('./webpack.config.js', webpack)))
    .pipe(plumber.stop())
    .pipe(gulp.dest(`./${outputDir}/`))
    .pipe(gulpIf(isDev, browserSync.stream()));
};

const buildPages = () => gulp.src(`./${inputDir}/*.hbs`)
  .pipe(plumber())
  .pipe(hb()
    // Partials
    .partials(`./${inputDir}/partials/**/*.hbs`)
    // Helpers
    .helpers(require('handlebars-helpers'))
    .helpers(require('handlebars-layouts'))
    .helpers(`./${inputDir}/helpers/**/*.js`)
    // Data
    .data(`./${inputDir}/data/**/*.{js,json}`)
    .data({
      publicPath: config.publicPath
    })
  )
  .pipe(rename({ extname: '.html' }))
  .pipe(plumber.stop())
  .pipe(gulp.dest(`./${outputDir}`))
  .pipe(gulpIf(isDev, browserSync.stream()));

const copyStatic = () => gulp.src(`./${inputDir}/static/**/*`)
  .pipe(gulp.dest(`./${outputDir}/static/`))
  .pipe(gulpIf(isDev, browserSync.stream()));

const devServer = () => browserSync.init({
  server: {
    baseDir: `./${outputDir}`
  },
  host: config.devServer.host,
  port: config.devServer.port
});

const meta = () => {
  let meta = {
    symbols: {},
    sprites: {}
  };

  const icons = gulp.src(`./${inputDir}/icons/{sprites,symbols}/**/*.svg`, { read: false })
    .pipe(tap(file => {
      const parsed = path.parse(file.relative);
      let key;
      let group = 'common';
      if (parsed.dir.indexOf(path.sep) >= 0) {
        key = path.dirname(parsed.dir);
        group = path.basename(parsed.dir);
      } else {
        key = parsed.dir;
      }
      if (typeof meta[key][group] === 'undefined') {
        meta[key][group] = [];
      }
      meta[key][group].push(parsed.name);
    }));

  return merge(icons)
    .on('end', () => {
      fs.writeFileSync(config.metaPath, JSON.stringify(meta));
    });
};

export const injectAssets = () => {
  const breakpoints = config.breakpoints || {};
  const sortableBreakpoints = Object.entries(breakpoints)
    .sort(([, a], [, b]) => a - b)
    .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});
  const breakpointValues = Object.values(sortableBreakpoints);

  return gulp.src(`./${outputDir}/*.html`)
    .pipe(inject(
      gulp
        .src(`./${outputDir}/**/vendor*.{js,css}`, { read: false })
        .pipe(gulp.src([
          `./${outputDir}/**/*.{js,css}`,
          `!./${outputDir}/**/vendor*.{js,css}`,
          `!./${outputDir}/static/**/*.{js,css}`,
          `!./${outputDir}/css/main-*.css`
        ], { read: false, passthrough: true }))
        .pipe(gulp.src(`./${outputDir}/css/main-*.css`, { read: false, passthrough: true, allowEmpty: true }))
        .pipe(sort((f1, f2) => {
          const m1 = path.basename(f1.path).match(/^main-(.+)\.css$/);
          const m2 = path.basename(f2.path).match(/^main-(.+)\.css$/);

          if (m1 && m2) {
            return sortableBreakpoints[m1[1]] > sortableBreakpoints[m2[1]] ? 1 : -1;
          }

          return 0;
        })),
      {
        relative: true,
        transform: function (filepath) {
          const ts = Date.now();
          if (isDev) {
            arguments[0] = filepath + '?v=' + ts;
          }

          const found = path.basename(filepath).match(/^main-(.+).css$/);
          if (found) {
            const breakpointName = found[1];
            const index = breakpointValues.findIndex(value => value === breakpoints[breakpointName]);
            return `<link rel="stylesheet" href="${filepath}${isDev ? '?v=' + ts : ''}" media="only screen and (min-width: ${breakpointValues[index]}px)">`;
          }
          return inject.transform.apply(inject.transform, arguments);
        }
      }))
    .pipe(gulp.dest(`./${outputDir}`));
};

export const build = gulp.series(
  clean,
  gulp.parallel(buildWebpack, meta, copyStatic),
  buildPages,
  injectAssets
);

const watch = () => {
  gulp.watch([
    `./${inputDir}/styles/**/*.scss`,
    `./${inputDir}/partials/blocks/**/*.scss`,
    `./${inputDir}/js/**/*.js`,
    `./${inputDir}/partials/blocks/**/*.js`
  ]).on('all', buildWebpack);
  gulp.watch([
    `./${inputDir}/data/**/*`,
    `./${inputDir}/helpers/**/*`,
    `./${inputDir}/partials/**/*`,
    `./${inputDir}/*.hbs`
  ]).on('all', gulp.series(buildPages, injectAssets));
  gulp.watch(`./${inputDir}/static/**/*`).on('all', copyStatic);
};

export const start = gulp.series(build, gulp.parallel(devServer, watch));
