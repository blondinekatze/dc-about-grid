module.exports = {
  pages: {
    form:{
      link: 'form.html',
      title: 'Форма с гибкими label'
    },
    columns:{
      link: 'columns.html',
      title: 'Пример разметки страницы'
    },
    gallery:{
      link: 'gallery.html',
      title: 'Обычная галерея (карточки в конце списка)'
    },
    masonry:{
      link: 'masonry.html',
      title: 'Галерея типа Масонри'
    },
    columnsAdaptive:{
      link: 'columns-adaptive.html',
      title: 'Адаптивные колонки без @media'
    },
    productCard:{
      link: 'product.html',
      title: 'Карточка товара в разных типах отображения'
    },
  }
}
